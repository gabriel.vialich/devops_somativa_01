
from pyrebase import pyrebase
#import pyrebase
import os
import stat
from datetime import datetime

firebaseConfig = {
    "apiKey": "AIzaSyDLe4h_iaUum-wEdcjD9UfmXmqXDm2vOqA",
    "authDomain": "eqp1firebase.firebaseapp.com",
    "projectId": "eqp1firebase",
    "databaseURL": "https://" + "eqp1firebase" + ".firebaseio.com",
    "storageBucket": "eqp1firebase.appspot.com",
    "messagingSenderId": "237244666040",
    "appId": "1:237244666040:web:2987d6f56a027be6c9e44d",
    "measurementId": "G-8SVNSL6C0H"
}

firebase = pyrebase.initialize_app(firebaseConfig)
auth = firebase.auth()

def Banco_de_Toquio():
    def autenticar_menu():
        print(10 * "-", "Banco de Tóquio", 10 * "-")
        print("1. Criar cadastro no Firebase. ")
        print("2. Enviar e-mail de verificação. ")
        print("3. Autenticar usuário no Firebase. ")
        print("4. Sair. ")
        print(61 * "-")

    loop = True
    while loop:
        autenticar_menu()
        escolha = input("Escolha uma opção [1-4]: ")

        if escolha == '1':
            user = input("Digite seu e-mail: ")
            password = input("Digite sua senha, com pelo menos 6 caracteres: ")
            status = auth.create_user_with_email_and_password(user, password)
            idToken = status["idToken"]
            #print("Resultado: ", status)
            #print("Token: ", idToken)
            print()
            print(61 * "-")
            print("Usuario " + user + " cadastrado com sucesso no Firebase!")
            print(61 * "-")
            print(61 * "-")
            auth.send_email_verification(idToken)
            print("Enviado email de verificação para " + user)
            print(61 * "-")
            print()
        elif escolha == '2':
            print()
            print(61 * "-")
            user = input("Digite seu e-mail: ")
            password = input("Digite sua senha, com pelo menos 6 caracteres: ")
            status = auth.sign_in_with_email_and_password(user, password)
            idToken = status["idToken"]
            auth.send_email_verification(idToken)
            print("Enviado email de verificação para " + user)
            print(61 * "-")
            print()
        elif escolha == '3':
            user = input("Digite seu e-mail: ")
            password = input("Digite sua senha, com pelo menos 6 caracteres: ")
            status = auth.sign_in_with_email_and_password(user, password)
            idToken = status["idToken"]
            info = auth.get_account_info(idToken)
            users = info["users"]
            verifyEmail = users[0]["emailVerified"]
            if verifyEmail:
                print()
                print(61 * "-")
                print("Você está autenticado!\n")
                print("Pega data e hora atual!\n")
                data_e_hora = datetime.now()
                data_e_hora_atual = data_e_hora.strftime('%d/%m/%Y %H:%M:%S')
                # print(data_e_hora_atual)
                print(61 * "-")
                print()
                print(61 * "-")
                print("Verifica se o arquivo \"acesso.txt\" existe!\n")
                print(61 * "-")
                if os.path.isfile("acesso.txt"):
                    # Modifica a permissão do arquivo para leitura, escrita e execução
                    os.chmod("acesso.txt", stat.S_IRWXU)
                    print("Atribui a permissão de leitura, escrita e execução no arquivo \"acesso.txt\"\n")

                print("Gravando  dados: " + user + " " + data_e_hora_atual + " no arquivo \"acesso.txt\"\n")
                # Abre o arquivo para escrita
                arquivo = open("acesso.txt", 'w')
                # Escreve no arquivo
                arquivo.write(user + " " + data_e_hora_atual)

                # Fecha o arquivo
                arquivo.close()

                # Modifica o arquivo apenas para leitura
                os.chmod("acesso.txt", stat.S_IRUSR)
                print("Fecha o arquivo \"acesso.txt\" e modifica permissão para leitura!\n")
                print(61 * "-")
                print()
            else:
                print("Usuário não autenticado! Confirmar email de verificação!\n")
        elif escolha == '4':
                print("Saindo...")
                loop = False
        else:
            input("Seleção de menu errada. Digite qualquer chave para tentar novamente ..")

print(Banco_de_Toquio())
